import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { PostService } from '../../services/post.service';
import { ImgurService } from '../../services/imgur.service';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnInit, OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;
    @Output() public removedPost: EventEmitter<number> = new EventEmitter<number>();

    public showComments = false;
    public showEditPost = false;
    public markAuthorPost = false;
    public imageUrl: string;
    public imageFile: File;
    public loading = false;
    public numberDislikes: number;
    public numberLikes: number;
    public userLikes: string[];
    public newComment = {} as NewComment;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private postService: PostService,
        private imgurService: ImgurService,
        private snackBarService: SnackBarService
    ) {}

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public ngOnInit() {
      this.markAuthorPost = this.currentUser === undefined ? false : this.currentUser.id === this.post.author.id;
      this.checkNumberLikes();
      this.checkUserLikes(true);
      this.checkUserLikes(false);
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public likePost(like: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp, like)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser, like)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));

        this.checkNumberLikes();
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }
    public togglePost() {
      if (!this.currentUser) {
        this.catchErrorWrapper(this.authService.getUser())
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe((user) => {
            if (user) {
              this.currentUser = user;
              this.showEditPost = !this.showEditPost;
            }
          });
        return;
      }

      this.showEditPost = !this.showEditPost;
    }
    public updatePost(post: Post) {
      const postSubscription = !this.imageFile
        ? this.postService.updatePost(this.post)
        : this.imgurService.uploadToImgur(this.imageFile, 'title').pipe(
          switchMap((imageData) => {
            this.post.previewImage = imageData.body.data.link;
            return this.postService.updatePost(this.post);
          })
        );

      this.loading = true;

      postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
        (respPost) => {
          this.loading = false;
        },
        (error) => this.snackBarService.showErrorMessage(error)
      );
      this.postService.updatePost(post);
      this.showEditPost = !this.showEditPost;
    }

    public removeImage() {
      this.post.previewImage = undefined;
      this.imageUrl = undefined;
      this.imageFile = undefined;
    }

    public loadImage(target: any) {
      this.imageFile = target.files[0];

      if (!this.imageFile) {
        target.value = '';
        return;
      }

      if (this.imageFile.size / 1000000 > 5) {
        target.value = '';
        this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
        return;
      }

      const reader = new FileReader();
      reader.addEventListener('load', () => (this.post.previewImage = reader.result as string));
      reader.readAsDataURL(this.imageFile);
    }

    public removePost() {
      this.postService.deletePost(this.post.id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (resp) => { this.removedPost.emit(this.post.id); },
          (error) => this.snackBarService.showErrorMessage(error)
      );
    }

    public checkUserLikes(like: boolean) {
      this.userLikes = this.post.reactions.filter((x) => x.isLike === like)
        .map(({user}) => user)
        .map(({userName}) => userName);
    }

    public removeComment(id: number) {
      this.post.comments = this.post.comments.filter((c) => c.id !== id);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

    private checkNumberLikes() {
    this.numberLikes = this.post.reactions.filter((x) => x.isLike === true).length;
    this.numberDislikes = this.post.reactions.filter((x) => x.isLike === false).length;
    }
}
