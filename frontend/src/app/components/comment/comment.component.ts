import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { User } from '../../models/user';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { CommentService } from '../../services/comment.service';
import { SnackBarService } from '../../services/snack-bar.service';
import { LikeService } from '../../services/like.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnInit {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    @Output() public removedComment: EventEmitter<number> = new EventEmitter<number>();

    public snowEditComments = false;
    public markAuthorComment = false;
    public numberDislikes: number;
    public numberLikes: number;
    public userLikes: string[];

    private unsubscribe$ = new Subject<void>();

    public constructor(
      private authService: AuthenticationService,
      private authDialogService: AuthDialogService,
      private commentService: CommentService,
      private snackBarService: SnackBarService,
      private likeService: LikeService
    ) {}

    public ngOnInit() {
      this.markAuthorComment = this.currentUser === undefined ? false : this.currentUser.id === this.comment.author.id;
      this.checkNumberLikes();
      this.checkUserLikes(true);
      this.checkUserLikes(false);
    }

    public toggleComments() {
      if (!this.currentUser) {
        this.catchErrorWrapper(this.authService.getUser())
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe((user) => {
            if (user) {
              this.currentUser = user;
              this.snowEditComments = !this.snowEditComments;
            }
          });
        return;
      }

      this.snowEditComments = !this.snowEditComments;
    }

    public removeComment() {
      this.commentService.deleteComment(this.comment.id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (resp) => { this.removedComment.emit(this.comment.id); },
          (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public updateComment() {
      this.commentService.updateComment(this.comment)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
        (respComment) => {
        },
        (error) => this.snackBarService.showErrorMessage(error)
      );
      this.commentService.updateComment(this.comment);
      this.snowEditComments = !this.snowEditComments;
    }

    public likeComment(like: boolean) {
      if (!this.currentUser) {
        this.catchErrorWrapper(this.authService.getUser())
          .pipe(
            switchMap((userResp) => this.likeService.likeComment(this.comment, userResp, like)),
            takeUntil(this.unsubscribe$)
          )
          .subscribe((comment) => (this.comment = comment));

        return;
      }

      this.likeService
        .likeComment(this.comment, this.currentUser, like)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((comment) => (this.comment = comment));

      this.checkNumberLikes();
    }

    public openAuthDialog() {
      this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public checkUserLikes(like: boolean) {
      this.userLikes = this.comment.reactions.filter((x) => x.isLike === like)
        .map(({user}) => user)
        .map(({userName}) => userName);
    }

    private catchErrorWrapper(obs: Observable<User>) {
      return obs.pipe(
        catchError(() => {
          this.openAuthDialog();
          return empty();
        })
      );
    }

    private checkNumberLikes() {
      this.numberLikes = this.comment.reactions.filter((x) => x.isLike === true).length;
      this.numberDislikes = this.comment.reactions.filter((x) => x.isLike === false).length;
    }
}
