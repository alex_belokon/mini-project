import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { LikesDialogComponent } from '../likes-dialog/likes-dialog.component';

@Component({
  selector: 'app-user-likes',
  templateUrl: './user-likes.component.html',
  styleUrls: ['./user-likes.component.css']
})
export class UserLikesComponent implements OnChanges, OnDestroy {

  @Output() public userLikesChange = new EventEmitter();

  @Input()
  get userLikes() {
    return this.innerUsers;
  }

  set userLikes(val) {
    this.innerUsers = val;
  }
  public innerUsers: string[];
  public firstUsers: string[];

  private unsubscribe$ = new Subject<void>();

  constructor(private dialog: MatDialog) {}

  public ngOnChanges(): void {
    this.firstUsers = this.userLikes.filter((i, index) => (index < 2));
  }

  public openDialog() {
    const dialog = this.dialog.open(LikesDialogComponent, {
      data: { users: this.userLikes },
      minWidth: 200,
      autoFocus: true,
    });

    dialog
      .afterClosed()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe();
  }

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
