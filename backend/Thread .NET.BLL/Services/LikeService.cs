﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;
        public LikeService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task LikePost(NewReactionDTO reaction)
        {
            var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId);

            if (likes.Any(x => x.IsLike == reaction.IsLike))
            {
                _context.PostReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                return;
            }

            if (likes.Any(x => x.IsLike != reaction.IsLike))
            {
                var modifyReaction = likes.FirstOrDefault();

                if (modifyReaction != null)
                {
                    modifyReaction.IsLike = reaction.IsLike;
                    modifyReaction.UpdatedAt = DateTime.Now;

                    _context.PostReactions.Attach(modifyReaction);

                    _context.Entry(modifyReaction).Property(c => c.IsLike).IsModified = true;
                }

                await _context.SaveChangesAsync();
                await _postHub.Clients.All.SendAsync("LikePost", reaction);

                return;
            }

            _context.PostReactions.Add(new DAL.Entities.PostReaction
            {
                PostId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
            await _postHub.Clients.All.SendAsync("LikePost", reaction);
        }

        public async Task LikeComment(NewReactionDTO reaction)
        {
            var likes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId);

            if (likes.Any(x => x.IsLike == reaction.IsLike))
            {
                _context.CommentReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                return;
            }

            if (likes.Any(x => x.IsLike != reaction.IsLike))
            {
                var modifyReaction = likes.FirstOrDefault();

                if (modifyReaction != null)
                {
                    modifyReaction.IsLike = reaction.IsLike;
                    modifyReaction.UpdatedAt = DateTime.Now;

                    _context.CommentReactions.Attach(modifyReaction);

                    _context.Entry(modifyReaction).Property(c => c.IsLike).IsModified = true;
                }

                await _context.SaveChangesAsync();
                await _postHub.Clients.All.SendAsync("LikeComment", reaction);

                return;
            }

            _context.CommentReactions.Add(new DAL.Entities.CommentReaction()
            {
                CommentId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
            await _postHub.Clients.All.SendAsync("LikeComment", reaction);
        }
    }
}
