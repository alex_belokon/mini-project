﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;
        public CommentService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task UpdateComment(CommentDTO commentDto)
        {
            var commentEntity = await GetCommentByIdInternal(commentDto.Id);
            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(Post), commentDto.Id);
            }

            commentEntity.Body = commentDto.Body;

            _context.Comments.Update(commentEntity);
            await _context.SaveChangesAsync();
            await _postHub.Clients.All.SendAsync("UpdateComment", commentDto);
        }

        public async Task DeleteComment(int commentId)
        {
            var commentEntity = await _context.Comments.FirstOrDefaultAsync(u => u.Id == commentId);

            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(Comment), commentId);
            }

            _context.Comments.Remove(commentEntity);
            await _context.SaveChangesAsync();
            await _postHub.Clients.All.SendAsync("DeleteComment", commentId);
        }

        private async Task<Comment> GetCommentByIdInternal(int id)
        {
            return await _context.Comments
                .FirstOrDefaultAsync(u => u.Id == id);
        }
    }
}
